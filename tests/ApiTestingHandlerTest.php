<?php

namespace TekoEstudio\ApiTesting\Test;

use TekoEstudio\ApiTesting\ApiTesting;
use TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException;
use TekoEstudio\ApiTesting\Handler\HandleTests;

class ApiTestingHandlerTest extends TestCaseBase
{
    /**
     * @return void
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     */
    public function testObject(): void
    {
        $this->assertInstanceOf(HandleTests::class,
            ApiTesting::handle($this->testGroup)
        );
    }

    /**
     * @return void
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     */
    public function testIncorrectGroup(): void
    {
        $this->expectException(TestsGroupsNotFoundException::class);
        ApiTesting::handle($this->faker->name());
    }

    /**
     * @return void
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     */
    public function testListing(): void
    {
        $apiTesting = ApiTesting::handle($this->testGroup);
        $this->assertIsArray($apiTesting->listerTests->getTestsList());
    }
}