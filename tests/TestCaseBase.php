<?php

namespace TekoEstudio\ApiTesting\Test;

use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use TekoEstudio\ApiTesting\Environments\PsrLoader;
use TekoEstudio\ApiTesting\Environments\TestEnvironment;

abstract class TestCaseBase extends TestCase
{
    /**
     * @var string
     */
    protected string $testGroup = 'endpoints';

    /**
     * @var \Faker\Generator
     */
    protected Generator $faker;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->configureTestEnvironment();
    }

    /**
     * @return void
     */
    private function configureTestEnvironment(): void
    {
        $psrLoader = new PsrLoader(
            'src/ExampleTests/',
            "TekoEstudio\\ApiTesting\\ExampleTests\\"
        );

        TestEnvironment::getInstance()->setPsrLoader($psrLoader);
    }
}