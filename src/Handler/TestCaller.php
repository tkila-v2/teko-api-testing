<?php

namespace TekoEstudio\ApiTesting\Handler;

use TekoEstudio\ApiTesting\Handler\Schemes\TestCaseResultsCollectionSchemes;
use TekoEstudio\ApiTesting\Results\TestCaseResult;
use TekoEstudio\ApiTesting\TestCases\TestCase;
use Throwable;

class TestCaller
{
    /**
     * @var \TekoEstudio\ApiTesting\TestCases\TestCase
     */
    protected TestCase $test;

    /**
     * Constructor.
     *
     * @param \TekoEstudio\ApiTesting\TestCases\TestCase $test
     */
    public function __construct(TestCase $test)
    {
        $this->test = $test;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestCaseResultsCollectionSchemes
     */
    public function execute(): TestCaseResultsCollectionSchemes
    {
        return $this->callTests();
    }

    /**
     * @return string
     */
    public function getTestsGroupName(): string
    {
        $testNamespaces = explode('\\', get_class($this->test));
        $lastKey        = array_key_last($testNamespaces);
        return $testNamespaces[$lastKey];
    }

    /**
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestCaseResultsCollectionSchemes
     */
    private function callTests(): TestCaseResultsCollectionSchemes
    {
        $testCaseResultsCollection = new TestCaseResultsCollectionSchemes($this->getTestsGroupName());

        foreach ($this->test->getTestsNames() as $testCaseName) {

            $testResult               = new TestCaseResult();
            $testResult->testCaseName = $testCaseName;
            $testResult->type         = $this->test->type;

            try {
                $this->test->{$testCaseName}();
                $testResult->passed();

            } catch (Throwable $throwable) {
                $testResult->addException($throwable);
            }

            $testCaseResultsCollection->addResult($testResult);
        }

        return $testCaseResultsCollection;
    }
}