<?php

namespace TekoEstudio\ApiTesting\Handler;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Environments\TestEnvironment;
use TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException;
use TekoEstudio\ApiTesting\Handler\Schemes\TestsCollectionScheme;
use TekoEstudio\ApiTesting\Schemes\TestGroupLoggerPrepare;
use TekoEstudio\ApiTesting\TestCases\TestGroup;

class ListerTests
{
    /**
     * @var string
     */
    protected string $testGroupName;

    /**
     * @var TestGroup
     */
    protected TestGroup $testGroupInstance;

    /**
     * Constructor.
     *
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     */
    public function __construct(string $testGroupName)
    {
        $this->testGroupName = $testGroupName;
        $this->prepareTestGroupInstance();
    }

    /**
     * @return void
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     */
    protected function prepareTestGroupInstance(): void
    {
        $testClassNamespace = TestEnvironment::getInstance()->getPsrLoader()->testsClassNamespace;
        $testsGroupClass    = $testClassNamespace . "TestGroupLogger";

        if (class_exists($testsGroupClass)) {
            $logger = new $testsGroupClass();

            if ($logger instanceof TestGroupLoggerPrepare) {
                $this->testGroupInstance = $logger->getTestInstanceByGroup($this->testGroupName);
                return;
            }
        }

        throw new TestsGroupsNotFoundException();
    }

    /**
     * @return array
     */
    #[Pure]
    public function getTestsList(): array
    {
        return $this->testGroupInstance->getTestsList();
    }

    /**
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestsCollectionScheme
     */
    public function getTestsSchemesCollection(): TestsCollectionScheme
    {
        return $this->testGroupInstance->getTestsCollectionScheme();
    }
}