<?php

namespace TekoEstudio\ApiTesting\Handler\Schemes;

use TekoEstudio\ApiTesting\Handler\ErrorsLogger;
use TekoEstudio\ApiTesting\Results\TestCaseResult;

class TestCaseResultsCollectionSchemes
{
    /**
     * @var array
     */
    private array $results = [];

    /**
     * @var string
     */
    public string $groupName;

    /**
     * @param string $groupName
     */
    public function __construct(string $groupName)
    {
        $this->groupName = $groupName;
    }

    /**
     * @param \TekoEstudio\ApiTesting\Results\TestCaseResult $result
     *
     * @return void
     */
    public function addResult(TestCaseResult $result)
    {
        $this->results[] = $result;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param \TekoEstudio\ApiTesting\Handler\ErrorsLogger $errorsLogger
     *
     * @return void
     */
    public function logger(ErrorsLogger $errorsLogger): void
    {
        foreach ($this->results as $key => $result) {
            if ($result instanceof TestCaseResult && $this->results[$key] instanceof TestCaseResult && !$result->isPass()) {
                $log = $errorsLogger->log($result);
                $this->results[$key]->setErrorLog($log);
            }
        }
    }
}