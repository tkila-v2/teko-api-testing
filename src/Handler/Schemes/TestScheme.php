<?php

namespace TekoEstudio\ApiTesting\Handler\Schemes;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Handler\TestCaller;
use TekoEstudio\ApiTesting\Results\TestCaseResult;
use TekoEstudio\ApiTesting\TestCases\TestCase;

class TestScheme
{
    /**
     * @var \TekoEstudio\ApiTesting\TestCases\TestCase
     */
    protected TestCase $test;

    /**
     * @var \TekoEstudio\ApiTesting\Results\TestCaseResult
     */
    protected TestCaseResult $result;

    /**
     * @var bool
     */
    private bool $handled = false;

    /**
     * @param \TekoEstudio\ApiTesting\TestCases\TestCase $test
     */
    #[Pure]
    public function __construct(TestCase $test)
    {
        $this->test   = $test;
        $this->result = new TestCaseResult();
    }

    /**
     * @return \TekoEstudio\ApiTesting\TestCases\TestCase
     */
    public function getTest(): TestCase
    {
        return $this->test;
    }

    /**
     * @return void
     */
    public function handled(): void
    {
        $this->handled = true;
    }

    /**
     * @return bool
     */
    public function isHandled(): bool
    {
        return $this->handled;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestCaseResultsCollectionSchemes
     */
    public function run(): TestCaseResultsCollectionSchemes
    {
        $testCaller = new TestCaller($this->getTest());
        $this->handled();

        return $testCaller->execute();
    }
}