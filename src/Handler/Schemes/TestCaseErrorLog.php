<?php

namespace TekoEstudio\ApiTesting\Handler\Schemes;

class TestCaseErrorLog
{
    /**
     * @param string $id
     * @param string $path
     */
    public function __construct(public string $id, public string $path) { }
}