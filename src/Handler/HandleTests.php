<?php

namespace TekoEstudio\ApiTesting\Handler;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Handler\Schemes\TestsCollectionScheme;

final class HandleTests
{
    /**
     * @var \TekoEstudio\ApiTesting\Handler\ListerTests
     */
    public ListerTests $listerTests;

    /**
     * @var \TekoEstudio\ApiTesting\Handler\Schemes\TestsCollectionScheme
     */
    private TestsCollectionScheme $testsCollectionScheme;

    /**
     * @var array
     */
    private array $testCasesResultsCollections = [];

    /**
     * @var \TekoEstudio\ApiTesting\Handler\ErrorsLogger
     */
    public ErrorsLogger $errorsLogger;

    /**
     * Constructor
     *
     * @param string $testGroup
     * @param bool   $clearLogs
     *
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     */
    public function __construct(string $testGroup, bool $clearLogs = false)
    {
        $this->errorsLogger          = new ErrorsLogger($clearLogs);
        $this->listerTests           = new ListerTests($testGroup);
        $this->testsCollectionScheme = $this->listerTests->getTestsSchemesCollection();
        $this->boot();
    }

    /**
     * @return void
     */
    private function boot(): void
    {
        pcntl_async_signals(false);
        pcntl_alarm(100000);
    }

    /**
     * @return int
     */
    #[Pure]
    public function getPreparedTestsNum(): int
    {
        return $this->testsCollectionScheme->getExecutableTestsNum();
    }

    /**
     * @return int
     */
    #[Pure]
    public function getUnpreparedTestsNum(): int
    {
        return $this->testsCollectionScheme->getIncompatibleTestsNum();
    }

    /**
     * @return void
     */
    public function handleAll(): void
    {
        do {
            $testScheme                  = $this->testsCollectionScheme->next();
            $testResultsCollectionScheme = $testScheme->run();

            $testResultsCollectionScheme->logger($this->errorsLogger);
            $this->testCasesResultsCollections[] = $testResultsCollectionScheme;
        } while ($this->testsCollectionScheme->isIterable());
    }

    /**
     * @return array
     */
    public function getTestCasesResultsCollections(): array
    {
        return $this->testCasesResultsCollections;
    }
}