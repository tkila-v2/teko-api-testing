<?php

namespace TekoEstudio\ApiTesting\Handler;

use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use TekoEstudio\ApiTesting\Environments\TestEnvironment;
use TekoEstudio\ApiTesting\Handler\Schemes\TestCaseErrorLog;
use TekoEstudio\ApiTesting\Results\TestCaseResult;

class ErrorsLogger
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var int
     */
    protected int $index = 0;

    /**
     * @var string
     */
    protected string $logsPath;

    /**
     * @var string
     */
    protected string $rootDir;

    /**
     * @var string
     */
    protected string $logsDir;

    /**
     * @var string
     */
    protected string $folder;

    /**
     * Constructor.
     *
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     */
    public function __construct(bool $clearLogs)
    {
        $this->id       = time();
        $this->rootDir  = TestEnvironment::getInstance()->getPsrLoader()->getRootDir();
        $this->logsDir  = TestEnvironment::getInstance()->getPsrLoader()->logsDir;
        $this->folder   = $this->rootDir . $this->logsDir;
        $this->logsPath = $this->folder . $this->id;

        $this->createLogsDir();

        // Clear olds logs folders
        if ($clearLogs) {
            $this->clear();
        }
    }

    /**
     * @return void
     */
    public function createLogsDir(): void
    {
        if (!is_dir($this->folder)) {
            mkdir($this->folder, 0700);
        }

        mkdir($this->logsPath, 0700);
    }

    /**
     * @param \TekoEstudio\ApiTesting\Results\TestCaseResult $testCaseResult
     *
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestCaseErrorLog
     */
    public function log(TestCaseResult $testCaseResult): TestCaseErrorLog
    {
        $logName  = $this->index++ . '_' . $testCaseResult->testCaseName;
        $filePath = $this->logsPath . $logName;
        $file     = fopen($filePath, 'w');

        $content = $testCaseResult->getException()->getMessage() . PHP_EOL . PHP_EOL;
        $content .= $testCaseResult->getException()->getTraceAsString();

        fwrite($file, $content);
        fclose($file);

        return new TestCaseErrorLog($logName, $filePath);
    }

    /**
     * @return void
     */
    public function clear(): void
    {
        $directory = new RecursiveDirectoryIterator($this->folder, FilesystemIterator::SKIP_DOTS);
        $files     = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $file) {
            is_dir($file) ? rmdir($file) : unlink($file);
        }
    }
}