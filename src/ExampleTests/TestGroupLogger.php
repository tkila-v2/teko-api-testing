<?php

namespace TekoEstudio\ApiTesting\ExampleTests;

use TekoEstudio\ApiTesting\ExampleTests\EndPoints\TestsGroup;
use TekoEstudio\ApiTesting\Schemes\TestGroupLoggerPrepare;

class TestGroupLogger extends TestGroupLoggerPrepare
{
    /**
     * @var array|string[]
     */
    public array $testsGroupNames = [
        'endpoints' => TestsGroup::class
    ];
}