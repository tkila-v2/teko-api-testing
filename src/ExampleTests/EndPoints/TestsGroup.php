<?php

namespace TekoEstudio\ApiTesting\ExampleTests\EndPoints;

use TekoEstudio\ApiTesting\ExampleTests\EndPoints\TestCases\Posts\GetPostsTest;
use TekoEstudio\ApiTesting\ExampleTests\EndPoints\TestCases\Posts\StorePostTest;
use TekoEstudio\ApiTesting\ExampleTests\EndPoints\TestCases\Posts\UpdatePostTest;
use TekoEstudio\ApiTesting\TestCases\TestGroup;

class TestsGroup extends TestGroup
{
    /**
     * @var string
     */
    protected string $route = 'https://jsonplaceholder.typicode.com/';

    /**
     * Test groups
     *
     * @var array
     */
    protected array $tests = [
        GetPostsTest::class,
        StorePostTest::class,
        UpdatePostTest::class
    ];

    /**
     * @return void
     */
    public function prepare(): void
    {
        // TODO.
    }
}