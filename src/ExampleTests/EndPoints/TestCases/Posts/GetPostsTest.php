<?php
/** @noinspection PhpUnused */

namespace TekoEstudio\ApiTesting\ExampleTests\EndPoints\TestCases\Posts;

use TekoEstudio\ApiTesting\Store\Stores\Credentials\Scheme\CredentialsScheme;
use TekoEstudio\ApiTesting\TestCases\EndPointTestCase;

class GetPostsTest extends EndPointTestCase
{
    /**
     * Test constructor
     */
    public function __construct() { }

    /**
     * @return void
     * @throws \Throwable
     */
    public function test_get_post_lists(): void
    {
        $json = [
            'name' => 'Felipe',
            'age'  => 21
        ];

        $this->store()->addCredential(new CredentialsScheme('tokean'));
    }
}