<?php
/** @noinspection PhpUnused */

namespace TekoEstudio\ApiTesting\ExampleTests\EndPoints\TestCases\Posts;

use TekoEstudio\ApiTesting\TestCases\EndPointTestCase;
use TekoEstudio\ApiTesting\Testers\Requests\Http\HttpStatusCodesEnum;

class StorePostTest extends EndPointTestCase
{
    /**
     * @var array
     */
    protected array $json = [
        'title'  => 'foo',
        'body'   => 'bar',
        'userId' => 1,
    ];

    /**
     * @inheritDoc
     */
    public function __construct() { }

    /**
     * @return void
     * @throws \Throwable
     */
    public function test_store_post(): void
    {
        $this->request()
             ->post('posts')->json($this->json)
             ->language('es')
             ->test()->assert()->status(HttpStatusCodesEnum::HTTP_CREATED)
             ->isJsonKeyValueEquals('title', 'foo')
             ->result()->dump()->getJson();
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function test_assert_json_exactly_scheme(): void
    {
        $this->json['id'] = 101;
        $this->request()
             ->post('posts')->json($this->json)
             ->test()->assert()->status(HttpStatusCodesEnum::HTTP_CREATED)
             ->isJsonEquals($this->json);
    }
}