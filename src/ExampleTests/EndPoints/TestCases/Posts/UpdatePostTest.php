<?php
/** @noinspection PhpUnused */

namespace TekoEstudio\ApiTesting\ExampleTests\EndPoints\TestCases\Posts;

use TekoEstudio\ApiTesting\TestCases\EndPointTestCase;
use TekoEstudio\ApiTesting\Testers\Requests\Http\HttpStatusCodesEnum;

class UpdatePostTest extends EndPointTestCase
{

    /**
     * @inheritDoc
     */
    public function __construct() { }

    /**
     * @return void
     * @throws \Throwable
     */
    public function test_update_post()
    {
        $json = [
            'title'  => 'boo',
            'body'   => 'lamp',
            'userId' => 2,
        ];

        $this->request()
             ->put('posts/1')->json($json)
             ->test()->assert()->status(HttpStatusCodesEnum::HTTP_OK)
             ->result();

        $this->dumper($this->store()->getCredential('tokean')->slug);
    }
}