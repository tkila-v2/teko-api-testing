<?php

namespace TekoEstudio\ApiTesting\Results\Types;

enum TestTypes
{
    case EndPoint;
    case Service;
    case TDD;
    case Integration;
    case Views;
    case Unit;
}