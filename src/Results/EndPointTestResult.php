<?php
/** @noinspection PhpUnused */

namespace TekoEstudio\ApiTesting\Results;

use Psr\Http\Message\ResponseInterface;
use TekoEstudio\ApiTesting\Asserts\ArraysAsserts;
use TekoEstudio\ApiTesting\Asserts\HttpAsserts;
use TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException;
use TekoEstudio\ApiTesting\Exceptions\EndPointResult\ResponseIsNotJsonException;
use TekoEstudio\ApiTesting\Exceptions\Testers\RequestIsAwaitForSendException;
use TekoEstudio\ApiTesting\Resolvers\DumperOutput;
use TekoEstudio\ApiTesting\Results\Types\TestTypes;
use Throwable;

class EndPointTestResult extends TestCaseResult
{
    use DumperOutput;

    /**
     * @var \Psr\Http\Message\ResponseInterface|null
     */
    protected ?ResponseInterface $response;

    /**
     * Constructor.
     *
     * @param \Psr\Http\Message\ResponseInterface|null $response
     * @param \Throwable|null                          $exception
     */
    public function __construct(?ResponseInterface $response = null, Throwable $exception = null)
    {
        $this->type     = TestTypes::EndPoint;
        $this->response = $response;

        if (!is_null($exception)) {
            $this->addException($exception);
        }
    }

    /**
     * Get response object
     *
     * @return \Psr\Http\Message\ResponseInterface|null
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     */
    public function getResponse(): ?ResponseInterface
    {
        return $this->response ?? throw new NotRequestAvailableException();
    }

    /**
     * @return int
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     */
    public function getStatusCode(): int
    {
        return $this->getResponse()->getStatusCode();
    }

    /**
     * Get response raw body
     *
     * @return string
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     */
    public function getBody(): string
    {
        $body = $this->getResponse()->getBody()->__toString();

        // Dump json array
        if ($this->isDump()) {
            $this->dumper($body);
        }

        return $body;
    }

    /**
     * @return array
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\ResponseIsNotJsonException
     */
    public function getJson(): array
    {
        $dump = $this->isDump();
        $this->unDump();

        $json = json_decode($this->getBody(), true);

        // When response is not json valid
        if (!is_array($json)) {
            throw new ResponseIsNotJsonException();
        }

        // Dump json array
        if ($dump) {
            $this->dumper($json);
        }

        return $json;
    }


    /**
     * @return \TekoEstudio\ApiTesting\Asserts\HttpAsserts
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     * @throws \TekoEstudio\ApiTesting\Exceptions\Testers\RequestIsAwaitForSendException
     */
    public function assert(): HttpAsserts
    {
        if (is_null($this->getResponse())) {
            throw new RequestIsAwaitForSendException();
        }

        return new HttpAsserts($this);
    }
}