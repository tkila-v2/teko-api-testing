<?php

namespace TekoEstudio\ApiTesting\Results;

use TekoEstudio\ApiTesting\Handler\Schemes\TestCaseErrorLog;
use TekoEstudio\ApiTesting\Results\Types\TestTypes;
use Throwable;

class TestCaseResult
{
    /**
     * @var \TekoEstudio\ApiTesting\Results\Types\TestTypes
     */
    public TestTypes $type;

    /**
     * @var bool
     */
    private bool $pass = false;

    /**
     * @var bool
     */
    private bool $dump = false;

    /**
     * @var mixed
     */
    public mixed $responseRaw = null;

    /**
     * @var string|null
     */
    public ?string $testCaseName = null;

    /**
     * @var \Throwable|null
     */
    private ?Throwable $exception = null;

    /**
     * @var TestCaseErrorLog|null
     */
    public ?TestCaseErrorLog $log = null;

    /**
     * @param \Throwable $throwable
     *
     * @return void
     */
    public function addException(Throwable $throwable): void
    {
        $this->exception = $throwable;
    }

    /**
     * @return \Throwable|null
     */
    public function getException(): ?Throwable
    {
        return $this->exception;
    }

    /**
     * @return void
     */
    public function passed(): void
    {
        $this->pass = true;
    }

    /**
     * @return bool
     */
    public function isPass(): bool
    {
        return $this->pass;
    }

    /**
     * @return bool
     */
    public function isDump(): bool
    {
        return $this->dump;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Results\TestCaseResult
     */
    public function dd(): static
    {
        $this->dump = true;
        return $this;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Results\TestCaseResult
     */
    public function dump(): static
    {
        return $this->dd();
    }

    /**
     * @return void
     */
    public function unDump(): void
    {
        $this->dump = false;
    }

    /**
     * @param \TekoEstudio\ApiTesting\Handler\Schemes\TestCaseErrorLog $caseErrorLog
     *
     * @return void
     */
    public function setErrorLog(TestCaseErrorLog $caseErrorLog)
    {
        $this->log = $caseErrorLog;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestCaseErrorLog|null
     */
    public function getErrorLog(): ?TestCaseErrorLog
    {
        return $this->log;
    }
}