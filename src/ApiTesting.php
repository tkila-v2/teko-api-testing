<?php

namespace TekoEstudio\ApiTesting;

use TekoEstudio\ApiTesting\Handler\HandleTests;

class ApiTesting
{
    /**
     * Get handler instance
     *
     * @param string $testGroup
     * @param bool   $clearLogs
     *
     * @return \TekoEstudio\ApiTesting\Handler\HandleTests
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     */
    static function handle(string $testGroup, bool $clearLogs = false): HandleTests
    {
        return new HandleTests($testGroup, $clearLogs);
    }
}