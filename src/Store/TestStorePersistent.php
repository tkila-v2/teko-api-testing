<?php

namespace TekoEstudio\ApiTesting\Store;


use TekoEstudio\ApiTesting\Store\Stores\Credentials\Scheme\CredentialsScheme;

class TestStorePersistent
{
    /**
     * @var array
     */
    private array $credentials = [];

    /**
     * @param \TekoEstudio\ApiTesting\Store\Stores\Credentials\Scheme\CredentialsScheme $scheme
     *
     * @return void
     */
    public function addCredential(CredentialsScheme $scheme)
    {
        $this->credentials[] = $scheme;
    }

    /**
     * @param string $slug
     *
     * @return \TekoEstudio\ApiTesting\Store\Stores\Credentials\Scheme\CredentialsScheme|null
     */
    public function getCredential(string $slug): ?CredentialsScheme
    {
        foreach ($this->credentials as $credential) {
            if ($credential instanceof CredentialsScheme && $credential->slug == $slug) {
                return $credential;
            }
        }

        return null;
    }
}