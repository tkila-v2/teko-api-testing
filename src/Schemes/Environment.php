<?php

namespace TekoEstudio\ApiTesting\Schemes;

use TekoEstudio\ApiTesting\Environments\PsrLoader;
use TekoEstudio\ApiTesting\Environments\TestCases\EndPointEnvironment;
use TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized;
use TekoEstudio\ApiTesting\Store\TestStorePersistent;
use TekoEstudio\ApiTesting\Traits\StorePersistent;

class Environment
{
    /**
     * @var \TekoEstudio\ApiTesting\Environments\PsrLoader|null
     */
    protected ?PsrLoader $psrLoader = null;

    /**
     * @var \TekoEstudio\ApiTesting\Store\TestStorePersistent
     */
    public TestStorePersistent $storePersistent;

    /**
     * @var \TekoEstudio\ApiTesting\Environments\TestCases\EndPointEnvironment|null
     */
    protected ?EndPointEnvironment $endPointEnvironment = null;

    /**
     * Set psr loader object
     *
     * @param \TekoEstudio\ApiTesting\Environments\PsrLoader $newPsrLoader
     *
     * @return void
     */
    public function setPsrLoader(PsrLoader $newPsrLoader)
    {
        $this->psrLoader ??= $newPsrLoader;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Environments\PsrLoader
     * @throws \TekoEstudio\ApiTesting\Exceptions\PsrLoader\PsrLoaderNotInitialized
     */
    public function getPsrLoader(): PsrLoader
    {
        return $this->psrLoader ?? throw new PsrLoaderNotInitialized();
    }

    /**
     * @param \TekoEstudio\ApiTesting\Environments\TestCases\EndPointEnvironment $endPointEnvironment
     *
     * @return void
     */
    public function setEndPointEnvironment(EndPointEnvironment $endPointEnvironment)
    {
        $this->endPointEnvironment ??= $endPointEnvironment;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Environments\TestCases\EndPointEnvironment|null
     */
    public function getEndPointEnvironment(): ?EndPointEnvironment
    {
        return $this->endPointEnvironment;
    }
}