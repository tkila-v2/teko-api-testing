<?php

namespace TekoEstudio\ApiTesting\Resolvers;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Resolvers\ConsoleOutput\OutputWithColor;

class ConsoleOutputFormats
{
    /**
     * @param string $srt
     *
     * @return \TekoEstudio\ApiTesting\Resolvers\ConsoleOutput\OutputWithColor
     */
    #[Pure]
    public static function write(string $srt): OutputWithColor
    {
        return new OutputWithColor($srt);
    }
}