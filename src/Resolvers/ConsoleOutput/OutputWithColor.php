<?php

namespace TekoEstudio\ApiTesting\Resolvers\ConsoleOutput;

class OutputWithColor
{
    /**
     * @param string $write
     */
    public function __construct(public string $write) { }

    /**
     * @param \TekoEstudio\ApiTesting\Resolvers\ConsoleOutput\ConsoleColorsCode $code
     *
     * @return void
     */
    public function color(ConsoleColorsCode $code): void
    {
        print($this->getColor($code));
    }

    /**
     * @param \TekoEstudio\ApiTesting\Resolvers\ConsoleOutput\ConsoleColorsCode $code
     *
     * @return string
     */
    public function getColor(ConsoleColorsCode $code): string
    {
        return ($code->value . $this->write . ConsoleColorsCode::Default->value);
    }

    /**
     * @return void
     */
    public function green(): void
    {
        $this->color(ConsoleColorsCode::Green);
    }

    /**
     * @return void
     */
    public function blue(): void
    {
        $this->color(ConsoleColorsCode::Blue);
    }

    /**
     * @return void
     */
    public function yellow(): void
    {
        $this->color(ConsoleColorsCode::Yellow);
    }

    /**
     * @return void
     */
    public function red(): void
    {
        $this->color(ConsoleColorsCode::Red);
    }
}