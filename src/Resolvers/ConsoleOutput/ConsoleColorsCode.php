<?php

namespace TekoEstudio\ApiTesting\Resolvers\ConsoleOutput;

enum ConsoleColorsCode: string
{
    case Red = "\e[31m";
    case Green = "\e[32m";
    case Yellow = "\e[33m";
    case Blue = "\e[34m";
    case Default = "\e[39m";
}