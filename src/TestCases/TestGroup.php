<?php

namespace TekoEstudio\ApiTesting\TestCases;

use TekoEstudio\ApiTesting\Environments\TestCases\EndPointEnvironment;
use TekoEstudio\ApiTesting\Environments\TestEnvironment;
use TekoEstudio\ApiTesting\Handler\Schemes\TestScheme;
use TekoEstudio\ApiTesting\Handler\Schemes\TestsCollectionScheme;
use Throwable;

abstract class TestGroup
{
    /**
     * @var string
     */
    protected string $route = 'https://jsonplaceholder.typicode.com/';

    /**
     * Test groups
     *
     * @var array
     */
    protected array $tests = [];

    /**
     * @var \TekoEstudio\ApiTesting\Handler\Schemes\TestsCollectionScheme|null
     */
    private ?TestsCollectionScheme $testsCollectionScheme = null;

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->loadBaseUrl();
        $this->prepare();
    }

    /**
     * @return void
     */
    public function prepare(): void
    {
        // TODO.
    }

    /**
     * @return void
     */
    private function loadBaseUrl(): void
    {
        $environment = new EndPointEnvironment($this->route);
        TestEnvironment::getInstance()->setEndPointEnvironment($environment);
    }

    /**
     * @return array
     */
    public function getTestsList(): array
    {
        return $this->tests;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestsCollectionScheme
     */
    public function getTestsCollectionScheme(): TestsCollectionScheme
    {
        if (!is_null($this->testsCollectionScheme)) {
            return $this->testsCollectionScheme;
        }

        $this->testsCollectionScheme = new TestsCollectionScheme();

        foreach ($this->tests as $test) {
            try {
                $testInstance = new $test();

                if ($testInstance instanceof TestCase) {
                    $testScheme = new TestScheme($testInstance);
                    $this->testsCollectionScheme->add($testScheme);
                }

            } catch (Throwable) {
                // TODO.
            }
        }

        return $this->testsCollectionScheme;
    }
}