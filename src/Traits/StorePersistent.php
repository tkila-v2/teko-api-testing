<?php

namespace TekoEstudio\ApiTesting\Traits;

use TekoEstudio\ApiTesting\Environments\TestEnvironment;
use TekoEstudio\ApiTesting\Store\TestStorePersistent;

trait StorePersistent
{
    /**
     * @return \TekoEstudio\ApiTesting\Store\TestStorePersistent
     */
    public function store(): TestStorePersistent
    {
        return TestEnvironment::getInstance()->storePersistent;
    }
}