<?php

namespace TekoEstudio\ApiTesting\Testers\Requests;

use TekoEstudio\ApiTesting\Results\EndPointTestResult;
use TekoEstudio\ApiTesting\Testers\Requests\Http\HttpClient;

class TestRequest extends HttpClient
{
    /**
     * @var \TekoEstudio\ApiTesting\Results\EndPointTestResult|null
     */
    protected ?EndPointTestResult $result = null;

    /**
     * Constructor.
     */
    public function __construct() { }

    /**
     * @return $this
     */
    public function self(): static
    {
        return $this;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Results\EndPointTestResult
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function test(): EndPointTestResult
    {
        $this->result = $this->build();
        return $this->result;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Results\EndPointTestResult
     */
    public function result(): EndPointTestResult
    {
        return $this->result;
    }
}