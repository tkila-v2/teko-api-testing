<?php

namespace TekoEstudio\ApiTesting\Testers\Requests\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use support\Log;
use TekoEstudio\ApiTesting\Results\EndPointTestResult;
use Throwable;

class HttpClient extends HttpMethods
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected Client $client;

    /**
     * @return \TekoEstudio\ApiTesting\Results\EndPointTestResult
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function build(): EndPointTestResult
    {
        try {
            $this->setClient();
            $response = $this->client->request($this->getMethod(), $this->getRouteUri(), $this->getOptions());
            return new EndPointTestResult($response);
        } catch (ClientException|RequestException $exception) {
            return new EndPointTestResult($exception->getResponse());
        }
    }

    /**
     * @return void
     */
    private function setClient(): void
    {
        $this->client = new Client([
            'base_uri' => $this->getBaseUri()
        ]);
    }
}