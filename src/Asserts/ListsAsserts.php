<?php
/** @noinspection PhpUnused */

namespace TekoEstudio\ApiTesting\Asserts;

use TekoEstudio\ApiTesting\Asserts\ArraysAsserts\ParseValue;
use TekoEstudio\ApiTesting\Asserts\ArraysAsserts\TypesOfValues;
use TekoEstudio\ApiTesting\Exceptions\Assertions\ArrayKeyValueComparedIsDifferent;
use TekoEstudio\ApiTesting\Exceptions\Assertions\ArraysIsDifferentException;
use TekoEstudio\ApiTesting\Exceptions\Assertions\KeyNotExistsInArrayException;

class ListsAsserts
{
    /**
     * @param array $structure
     * @param array $array
     * @param array $errors
     *
     * @return void
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\ArraysIsDifferentException
     */
    public function isArraysStructureEquals(array $structure, array $array, array &$errors = []): void
    {
        foreach ($structure as $key => $value) {
            if (is_array($value)) {
                if (!array_key_exists($key, $array)) {
                    $errors[] = [$key => false];
                }
                $this->isArraysStructureEquals($structure[$key], $array[$key], $errors);
            } else if (!array_key_exists($value, $array)) {
                $errors[] = [$key => false];
            }
        }

        if (!empty($errors)) {
            throw new ArraysIsDifferentException();
        }
    }

    /**
     * @param array $arrayOne
     * @param array $arrayTwo
     *
     * @return $this
     * @throws \JsonException
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\ArraysIsDifferentException
     */
    public function isArrayStrictEquals(array $arrayOne, array $arrayTwo): static
    {
        if (json_encode($arrayOne, JSON_THROW_ON_ERROR) !== json_encode($arrayTwo, JSON_THROW_ON_ERROR)) {
            throw new ArraysIsDifferentException();
        }

        return $this;
    }

    /**
     * @param array  $array
     * @param string $key
     *
     * @return \TekoEstudio\ApiTesting\Asserts\ListsAsserts
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\KeyNotExistsInArrayException
     */
    public function keyExists(array $array, string $key): static
    {
        $this->getArrayValueFromKey($array, $key);
        return $this;
    }

    /**
     * @param array  $array
     * @param string $key
     *
     * @return array|mixed
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\KeyNotExistsInArrayException
     */
    public function getArrayValueFromKey(array $array, string $key): mixed
    {
        $keys = explode('.', $key);

        foreach ($keys as $index) {
            array_key_exists($index, $array) ?
                $array = $array[$index] :
                throw new KeyNotExistsInArrayException($index);
        }

        return $array;
    }

    /**
     * @param array  $array
     * @param string $key
     * @param mixed  $value
     *
     * @return \TekoEstudio\ApiTesting\Asserts\ListsAsserts
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\ArrayKeyValueComparedIsDifferent
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\KeyNotExistsInArrayException
     */
    public function valueIsEquals(array $array, string $key, mixed $value): static
    {
        $arrayValue = $this->getArrayValueFromKey($array, $key);

        if ($arrayValue !== $value) {
            throw new ArrayKeyValueComparedIsDifferent($key);
        }

        return $this;
    }

    /**
     * @param array                                                       $array
     * @param string                                                      $keys
     * @param \TekoEstudio\ApiTesting\Asserts\ArraysAsserts\TypesOfValues $type
     *
     * @return \TekoEstudio\ApiTesting\Asserts\ListsAsserts
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\KeyNotExistsInArrayException
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\KeyValueTypeIsNotExceptedException
     */
    public function valueHasType(array $array, string $keys, TypesOfValues $type): static
    {
        $value     = $this->getArrayValueFromKey($array, $keys);
        $converter = new ParseValue($keys, $value);
        $converter->fromType($type);

        return $this;
    }

    /**
     * @param array $parentArray
     * @param array $fragmentArray
     *
     * @return $this
     */
    public function isArrayFragmentStructureEquals(array $parentArray, array $fragmentArray): static
    {
        array_map(static function ($element) {
            var_dump($element);
            echo PHP_EOL . '----------------' . PHP_EOL;
        }, $parentArray);

        return $this;
    }
}
