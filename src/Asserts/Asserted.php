<?php

namespace TekoEstudio\ApiTesting\Asserts;

use JetBrains\PhpStorm\Pure;

class Asserted
{
    /**
     * @return \TekoEstudio\ApiTesting\Asserts\ListsAsserts
     */
    #[Pure]
    public function arrays(): ListsAsserts
    {
        return new ListsAsserts();
    }

    /**
     * @return void
     */
    public function staticAsserts()
    {
    }
}