<?php

namespace TekoEstudio\ApiTesting\Asserts;

use TekoEstudio\ApiTesting\Asserts\ArraysAsserts\TypesOfValues;
use TekoEstudio\ApiTesting\Exceptions\Assertions\ArraysIsDifferentException;
use TekoEstudio\ApiTesting\Exceptions\Assertions\JsonResultIsDifferentException;
use TekoEstudio\ApiTesting\Exceptions\Assertions\StatusCodeDifferentException;
use TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException;
use TekoEstudio\ApiTesting\Exceptions\EndPointResult\ResponseIsNotJsonException;
use TekoEstudio\ApiTesting\Results\EndPointTestResult;
use TekoEstudio\ApiTesting\Testers\Requests\Http\HttpStatusCodesEnum;

class HttpAsserts extends ListsAsserts
{
    /**
     * @param \TekoEstudio\ApiTesting\Results\EndPointTestResult $result
     */
    public function __construct(public EndPointTestResult $result) { }

    /**
     * @param \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpStatusCodesEnum $statusCode
     *
     * @return \TekoEstudio\ApiTesting\Asserts\HttpAsserts
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\StatusCodeDifferentException
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     */
    public function status(HttpStatusCodesEnum $statusCode): static
    {
        $resultStatusCode   = $this->result->getStatusCode();
        $exceptedStatusCode = $statusCode->value;

        if ($resultStatusCode != $exceptedStatusCode) {
            throw new StatusCodeDifferentException($resultStatusCode, $exceptedStatusCode);
        }

        return $this;
    }

    /**
     * @param array $json
     *
     * @return \TekoEstudio\ApiTesting\Asserts\HttpAsserts
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\JsonResultIsDifferentException
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\ResponseIsNotJsonException
     */
    public function isJsonEquals(array $json): static
    {
        try {
            $this->isArrayStrictEquals($json, $this->result->getJson());
        } catch (ArraysIsDifferentException) {
            throw new JsonResultIsDifferentException();
        }

        return $this;
    }

    /**
     * @param string $keys
     * @param mixed  $value
     *
     * @return static
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\ArrayKeyValueComparedIsDifferent
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\KeyNotExistsInArrayException
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\NotRequestAvailableException
     * @throws \TekoEstudio\ApiTesting\Exceptions\EndPointResult\ResponseIsNotJsonException
     */
    public function isJsonKeyValueEquals(string $keys, mixed $value): static
    {
        $this->valueIsEquals($this->result()->getJson(), $keys, $value);
        return $this;
    }

    /**
     * @return \TekoEstudio\ApiTesting\Results\EndPointTestResult
     */
    public function result(): EndPointTestResult
    {
        return $this->result;
    }
}