<?php

namespace TekoEstudio\ApiTesting\Asserts\ArraysAsserts;

use Assert\Assertion;
use Assert\AssertionFailedException;
use TekoEstudio\ApiTesting\Exceptions\Assertions\KeyValueTypeIsNotExceptedException;

class ParseValue
{
    /**
     * @param string $keys
     * @param mixed  $value
     */
    public function __construct(public string $keys, public mixed $value) { }

    /**
     * @param \TekoEstudio\ApiTesting\Asserts\ArraysAsserts\TypesOfValues $type
     *
     * @return void
     * @throws \TekoEstudio\ApiTesting\Exceptions\Assertions\KeyValueTypeIsNotExceptedException
     */
    public function fromType(TypesOfValues $type): void
    {
        try {
            match ($type) {
                TypesOfValues::STRING => Assertion::string($this->value),
                TypesOfValues::INT => Assertion::integer($this->value),
                TypesOfValues::ARRAY => Assertion::isArray($this->value),
                TypesOfValues::FLOAT => Assertion::float($this->value),
                TypesOfValues::BOOL => Assertion::boolean($this->value),
                TypesOfValues::NULL => Assertion::null($this->value),
            };
        } catch (AssertionFailedException $e) {
            throw new KeyValueTypeIsNotExceptedException($this->keys, $e->getMessage());
        }
    }
}