<?php

namespace TekoEstudio\ApiTesting\Asserts\ArraysAsserts;

enum TypesOfValues: string
{
    case STRING = 'string';
    case INT = 'integer';
    case FLOAT = 'float';
    case ARRAY = 'array';
    case BOOL = 'boolean';
    case NULL = 'null';
}