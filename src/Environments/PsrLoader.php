<?php

namespace TekoEstudio\ApiTesting\Environments;

use Composer\Autoload\ClassLoader;
use ReflectionClass;

class PsrLoader
{
    /**
     * @param string $testDir
     * @param string $logsDir
     * @param string $testsClassNamespace
     */
    public function __construct(
        public string $testDir,
        public string $logsDir,
        public string $testsClassNamespace
    )
    {
        // TODO.
    }

    /**
     * @return string|null
     */
    public function getRootDir(): ?string
    {
        $loader     = new ClassLoader();
        $reflection = new ReflectionClass($loader);

        $file      = $reflection->getFileName();
        $directory = dirname($file);

        return $this->iterableVendorDir($directory);
    }

    /**
     * @param string $vendorDir
     *
     * @return string|null
     */
    private function iterableVendorDir(string $vendorDir): ?string
    {
        $vendorDirsArr = explode('/', $vendorDir);
        $directory     = '';

        foreach ($vendorDirsArr as $dir) {

            if ($dir == 'vendor') {
                return $directory;
            }

            $directory .= $dir . '/';
        }

        return null;
    }
}