<?php

namespace TekoEstudio\ApiTesting\Environments\TestCases;

class EndPointEnvironment
{
    /**
     * Constructor.
     */
    public function __construct(
        public ?string $route = null
    )
    {
        // TODO.
    }
}