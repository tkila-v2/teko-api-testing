<?php

namespace TekoEstudio\ApiTesting\Exceptions\EndPointResult;

use JetBrains\PhpStorm\Pure;

class ResponseIsNotJsonException extends EndPointResultException
{
    /**
     * Exception constructor.
     */
    #[Pure]
    public function __construct()
    {
        parent::__construct('Request response is not parseable to json array');
    }
}