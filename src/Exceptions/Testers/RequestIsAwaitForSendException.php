<?php

namespace TekoEstudio\ApiTesting\Exceptions\Testers;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Exceptions\TestException;

class RequestIsAwaitForSendException extends TestException
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('Is required send first the request before use an assert');
    }
}