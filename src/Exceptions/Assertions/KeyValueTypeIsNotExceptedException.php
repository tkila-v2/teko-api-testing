<?php

namespace TekoEstudio\ApiTesting\Exceptions\Assertions;

use JetBrains\PhpStorm\Pure;

class KeyValueTypeIsNotExceptedException extends AssertException
{
    /**
     * @param string $key
     * @param string $message
     */
    #[Pure]
    public function __construct(string $key, string $message)
    {
        parent::__construct("Key: $key. $message");
    }
}