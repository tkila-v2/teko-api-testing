<?php

namespace TekoEstudio\ApiTesting\Exceptions\Assertions;

use JetBrains\PhpStorm\Pure;

class ArraysIsDifferentException extends AssertException
{
    /**
     * Exception constructor.
     */
    #[Pure]
    public function __construct()
    {
        parent::__construct('The arrays are different');
    }
}