<?php

namespace TekoEstudio\ApiTesting\Exceptions\Assertions;

use JetBrains\PhpStorm\Pure;
use Throwable;

class KeyNotExistsInArrayException extends AssertException
{
    /**
     * @param string $key
     */
    #[Pure]
    public function __construct(string $key)
    {
        parent::__construct("The key $key not exists in array");
    }
}