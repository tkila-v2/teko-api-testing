<?php

namespace TekoEstudio\ApiTesting\Exceptions\Assertions;

use JetBrains\PhpStorm\Pure;

class StatusCodeDifferentException extends AssertException
{
    #[Pure]
    public function __construct(int $resultStatusCode, int $exceptedStatusCode)
    {
        parent::__construct("Status code incorrect, received $resultStatusCode, excepted $exceptedStatusCode");
    }
}