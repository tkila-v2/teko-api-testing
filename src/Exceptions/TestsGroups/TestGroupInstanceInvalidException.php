<?php

namespace TekoEstudio\ApiTesting\Exceptions\TestsGroups;

use TekoEstudio\ApiTesting\Exceptions\TestException;

class TestGroupInstanceInvalidException extends TestException
{

}