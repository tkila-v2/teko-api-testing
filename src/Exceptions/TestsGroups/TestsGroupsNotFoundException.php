<?php

namespace TekoEstudio\ApiTesting\Exceptions\TestsGroups;

use TekoEstudio\ApiTesting\Exceptions\TestException;

class TestsGroupsNotFoundException extends TestException
{

}