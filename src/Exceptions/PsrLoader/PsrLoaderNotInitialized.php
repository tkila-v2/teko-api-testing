<?php

namespace TekoEstudio\ApiTesting\Exceptions\PsrLoader;

use TekoEstudio\ApiTesting\Exceptions\TestException;

class PsrLoaderNotInitialized extends TestException
{

}