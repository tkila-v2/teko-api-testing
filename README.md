# Teko Api Testing

## Getting started

Teko Api Testing is a tool that will allow you to carry out effective integration tests for webservices such as api rest
full, which will guarantee the quality of your services, making a correctly tested development available to everyone,
avoiding and detecting possible failures in time before Launch your project into production.

## How works?

If the library is installed alongside Tkila, you can run the tests with the following command

```
php webman tests:run --group=endpoints --fresh=true
```

In case you want to run tests in CLI, you must execute the following auxiliary command

```
php teko_api_testing <your_test_group_her>
```

## Test this library

This test library guarantees API tests, despite this, there are also tests performed on this library, so you can run
them with the following command:

```
composer run-script test
```

with ❤️ for [Teko Estudio](https://tekoestudio.com/)